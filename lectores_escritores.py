from threading import *
import time
class Recurso:
	def __init__(self):
		self.lectores = 0
		self.escritores = 0
		self.leer_lock = Lock()
		self.permiso_leer = Condition(self.leer_lock)
		self.escribir_lock = Lock()
		self.permiso_escribir = Condition(self.escribir_lock)
		self.escribiendo = False
		self.leyendo = False
	def espera_lectura(self):
		self.lectores += 1
		with self.leer_lock:
			while self.escribiendo:
				self.permiso_leer.wait() #Se espera hasta que se de permiso para leer
			self.leyendo = True
	def fin_lectura(self):
		self.lectores -= 1
		with self.escribir_lock:
			if self.lectores == 0:
				self.permiso_escribir.notify() #Se le avisa a 1 que el recurso está disponible
				self.leyendo = False

	def espera_escritura(self):
		self.escritores += 1
		with self.escribir_lock:
			while self.leyendo or self.escribiendo:
				self.permiso_escribir.wait() #Esperamos a que no hayan escritores
			self.escribiendo = True

	def fin_escritura(self):
		self.escritores -= 1
		self.escribiendo = False
		if self.escritores != 0:
			with self.escribir_lock:
				self.permiso_escribir.notify() #Se le avisa a otro escritor que se puede escribir
		else:
			with self.leer_lock:
				self.permiso_leer.notifyAll() #Se le avisa a todos los lectores que ya se puede leer


class Lector(Thread):
	def __init__(self, index, recurso, tiempo):
		Thread.__init__(self)
		self.index = index
		self.recurso = recurso
		self.tiempo = tiempo
	def run(self):
		self.leer()
	def leer(self):
		self.recurso.espera_lectura()
		print("Lector "+str(self.index)+" está leyendo.")
		time.sleep(self.tiempo)
		print("Lector "+str(self.index)+" terminó de leer.")
		self.recurso.fin_lectura()

class Escritor(Thread):
	def __init__(self, index, recurso, tiempo):
		Thread.__init__(self)
		self.index = index
		self.recurso = recurso
		self.tiempo = tiempo
	def run(self):
		self.escribir()
	def escribir(self):
		self.recurso.espera_escritura()
		print("Escritor "+str(self.index)+" está escribiendo")
		time.sleep(self.tiempo)
		print("Escritor "+str(self.index)+" ha terminado de escribir.")
		self.recurso.fin_escritura()

r = Recurso()
Lector(1, r, 1).start()
Escritor(1, r, 1).start()
Lector(2, r, 1).start()
time.sleep(2.1)
Lector(3, r, 1).start()
Escritor(2, r, 1).start()
time.sleep(2.1)
Escritor(3, r, 2).start()
Lector(4, r, 1).start()
Lector(5, r, 1).start()